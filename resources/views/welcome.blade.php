<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>
        #chatBoard {
            width: 500px;
        }
        #chatBoard .chat {
            overflow: hidden;
            margin-bottom: 10px;
        }
        #chatBoard .chat .bot {
            background: #a3ffd7;
            float: left;
            padding: 10px;
        }
        #chatBoard .chat .human {
            background: #6fb8fc;
            float: right;
            padding: 10px;
        }
        #chatBoard button {
            padding: 5px;
            margin: 2px;
        }
        </style>
        <title>Dialog Flow</title>
    </head>
    <body>
        <div id="chatBoard">

        </div>
    </body>
</html>

<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script>
$(document).ready(function() {
    var url = "{{ route('api.dialog') }}";

    function RunBot(url) {
        $.get(url)
        .done(function( data ) {
            var html_answer_option = '';
            $.each(data.answers, function(index, answer) {
                html_answer_option += `<button class="answer" action="` + answer.action + ` ">` 
                                        + answer.text + 
                                    `</button>`;
            });

            var html_chat = `<div class="bot">
                                <div class="text">`
                                + data.text + 
                                `</div>
                                <div class="answer_option">`
                                + html_answer_option +
                                `</div>
                            </div>`;

            $('#chatBoard').append(`<div class="chat">`+html_chat+`</div>`);
            console.log(data);
        });
    }

    RunBot(url);

    $(document).on('click', '#chatBoard button.answer', function() {
        var html_chat = `<div class="human">
                                <div class="text">`
                                + $(this).html() + 
                                `</div>
                            </div>`;

        $('#chatBoard').append(`<div class="chat">`+html_chat+`</div>`);
        RunBot($(this).attr('action'));
    });
});
</script>
