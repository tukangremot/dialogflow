<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class TableDialogsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dialogs')->insert([
            [
                'id' => 1,
                'text' => 'Hallo, Selamat Datang {nama}. Kategori apa yang ingin anda tanyakan?',
                'dialog_type' => 'question',
                'parent' => NULL
            ], [
                'id' => 2,
                'text' => 'Pengadaan',
                'dialog_type' => 'answer',
                'parent' => 1
            ], [
                'id' => 3,
                'text' => 'Penawaran',
                'dialog_type' => 'answer',
                'parent' => 1
            ], [
                'id' => 4,
                'text' => 'Apa jenis pengadaannya?',
                'dialog_type' => 'question',
                'parent' => 2
            ], [
                'id' => 5,
                'text' => 'Pengadaan Barang',
                'dialog_type' => 'answer',
                'parent' => 4
            ], [
                'id' => 6,
                'text' => 'Pengadaan Pekerjaan Konstruksi',
                'dialog_type' => 'answer',
                'parent' => 4
            ], [
                'id' => 7,
                'text' => 'Pengadaan Jasa',
                'dialog_type' => 'answer',
                'parent' => 4
            ]
        ]);
    }
}
