<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDialogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dialogs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('text');
            $table->unsignedBigInteger('parent')->nullable();
            $table->unsignedBigInteger('parent_yes')->nullable();
            $table->unsignedBigInteger('parent_no')->nullable();
            $table->string('dialog_type', 20);

            $table->timestamps();
            $table->foreign('parent')->references('id')->on('dialogs');
            $table->foreign('parent_yes')->references('id')->on('dialogs');
            $table->foreign('parent_no')->references('id')->on('dialogs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dialogs');
    }
}
