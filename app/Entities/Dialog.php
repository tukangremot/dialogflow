<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Dialog extends Model {

    public function answers() {
        return $this->hasMany(Dialog::class, 'parent');
    }

    public function question() {
        return $this->hasOne(Dialog::class, 'parent');
    }

}
