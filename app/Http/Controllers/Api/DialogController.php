<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Dialog;

class DialogController extends Controller {

    public function get(Request $request) {
        $dialog_id = 1;

        if ($request->id)
            $dialog_id = $request->id;

        $dialog = Dialog::find($dialog_id);

        $result = [
            'id' => $dialog->id,
            'text' => $dialog->text,
            'type' => $dialog->dialog_type
        ];

        if ($dialog->dialog_type == 'question') {
            $answers = [];
            foreach($dialog->answers as $answer) {
                $answers[] = [
                    'id' => $answer->id,
                    'text' => $answer->text,
                    'dialog_type' => $answer->dialog_type,
                    'action' => $answer->question['id'] ?
                        route('api.dialog', ['id' => $answer->question['id']]) : NULL
                ];
            }
            $result['answers'] = $answers;
        }
        
        return response()->json($result);
    }

}
